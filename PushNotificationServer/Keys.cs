﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PushNotificationServer
{
    public static class Keys
    {
        /// <summary>
        /// For test purposes just hardcode this value.
        /// But in real world the client app needs to somehow send this value to the server.
        /// </summary>
        public static string PushNotificationUrl = "https://hk2.notify.windows.com/?token=AwYAAADiCwZLSvaUNmxPKT0kRPMDNzULGNTvu9OvgrOVLVN%2bA92zt1SmWdGOkClmZq6bNPG8JF6wR5qk9x5KPvanKW4MBxm0NM9MrrEnbAtSL8UgYEeYNvA75Y7skBNkXyUpY94%3d";

        /// <summary>
        /// You need to get this value from your developer dashboard.
        /// </summary>
        public static string PackageSID = "ms-app://s-1-15-2-3530210685-1641594596-1698141586-1936742293-2866449052-702142287-1110044900";

        /// <summary>
        /// You need to get this value from your developer dashboard.
        /// </summary>
        public static string SecretKey = "T3R+fMTZX2GvjXgxbAWZ8gLBVw1jf/v6";
    }
}
