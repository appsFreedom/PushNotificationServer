﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PushNotificationServer
{
    class Program
    {
        static void Main(string[] args)
        {
            MainAsync().Wait();
        }

        static async Task MainAsync()
        {
            var pushNotificationService = new PushNotificationService();
            await pushNotificationService.SendPushNotification("AppsFreedom test!");
        }
    }
}
