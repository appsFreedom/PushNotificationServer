﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace PushNotificationServer
{
    public class PushNotificationService
    {
        public async Task SendPushNotification(string content)
        {
            string accessToken = await GetAccessTokenAsync();

            Uri uri = new Uri(Keys.PushNotificationUrl);

            HttpClient httpClient = new HttpClient();

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, uri);
            request.Headers.Add("X-WNS-Type", "wns/raw");
            request.Headers.Add("Authorization", String.Format("Bearer {0}", accessToken));
            request.Content = new StringContent(content);
            request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

            var response = await httpClient.SendAsync(request);
        }

        private async Task<string> GetAccessTokenAsync()
        {
            Uri uri = new Uri(ACCESS_TOKEN_URL);

            var httpContent = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("grant_type", "client_credentials"),
                new KeyValuePair<string, string>("client_id", Keys.PackageSID),
                new KeyValuePair<string, string>("client_secret", Keys.SecretKey),
                new KeyValuePair<string, string>("scope", "notify.windows.com"),
            });

            HttpClient httpClient = new HttpClient();
            var result = await httpClient.PostAsync(uri, httpContent);
            var str = await result.Content.ReadAsStringAsync();

            var obj = JsonConvert.DeserializeObject<GetAccessTokenResponse>(str);
            return obj != null ? obj.AccessToken : null;
        }

        private const string ACCESS_TOKEN_URL = "https://login.live.com/accesstoken.srf";
    }
}
